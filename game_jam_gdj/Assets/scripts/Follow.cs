using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Follow : MonoBehaviour
{
    public bool isPlay = false;
    public GameObject target;

    public float xValue;
    public float yValue;
    private bool islaunch = true;
    public GameObject positionMeteor;
    private bool isWaiting = false;
    private float timeLeft = 4;
    public ArrayList meteorsPrefab;
    public float timeToGameOver = 45;
    private Color targetColor;
    public GameObject colorToShoot;

    public float refreshLaunch = 2;

    [SerializeField] private Camera mainCamera;
    public Vector3 screenPosition;
    public Vector3 worldPosition;
    public float zPosition = 50;
    public GameObject draft;
    public Color[] colors = { Color.red, Color.blue, Color.green };
    public GameObject[] meteoresForm;

    private GameObject[] meteorsToDelete;

    void Update()
    {

        if (Input.GetKeyDown("space"))
        {
            if (!isPlay)
            {

                restartGame();
                meteorsToDelete = GameObject.FindGameObjectsWithTag("METEOR");
                foreach (GameObject meteorTodelete in meteorsToDelete)
                {
                    GameObject.Destroy(meteorTodelete);
                }
            }

        }
        if (isPlay)
        {
            if (isWaiting)
            {
                timeLeft -= Time.deltaTime;

                if (timeLeft < 0)
                {
                    islaunch = true;
                    isWaiting = false;
                    timeLeft = refreshLaunch;
                }
            }

            if (islaunch)
            {
                launchFunction();
            }


            clickShootFunction();
            transform.LookAt(target.transform);

        }
        else
        {
            transform.rotation = Quaternion.identity;
        }

        void clickShootFunction()
        {

            screenPosition = Input.mousePosition;
            screenPosition.z = Camera.main.nearClipPlane + zPosition;

            worldPosition = Camera.main.ScreenToWorldPoint(screenPosition);
            target.transform.position = worldPosition;

            zPosition = zPosition + Input.mouseScrollDelta.y;

            if (Input.GetMouseButtonUp(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 200.0f) && hit.transform.gameObject != null && hit.transform.gameObject.tag == "METEOR" &&
                    colorToShoot.GetComponent<Renderer>().material.GetColor("_color") == hit.transform.gameObject.GetComponent<Renderer>().material.GetColor("_color")
                    )
                {
                    GameObject.Destroy(hit.transform.gameObject);
                    restartGame();
                }
                else
                {
                    isPlay = false;

                }

                Vector3 pos = Input.mousePosition;
                pos.z = 50;
                pos = Camera.main.ScreenToWorldPoint(pos);
                Instantiate(draft, pos, Quaternion.Euler(90, 180, 0));
            }


        }
    }

    void restartGame()
    {
        isPlay = true;
        isWaiting = true;
        islaunch = false;
        timeLeft = refreshLaunch;
        timeToGameOver = 45;
        targetColor = colors[Random.Range(0, 3)];
        colorToShoot.GetComponent<Renderer>().material.SetColor("_Color", targetColor);
    }

   void  launchFunction()
    {
        timeToGameOver -= Time.deltaTime;
        if (timeToGameOver < 0)
        {
            isPlay = false;
        }
        int random = Random.Range(0, 2);
        int randomForm = Random.Range(0, meteoresForm.Length);
        Vector3 newPosition = positionMeteor.transform.position;
        GameObject newMeteor = Instantiate(meteoresForm[randomForm], newPosition, Quaternion.identity);
        newMeteor.tag = "METEOR";

        newMeteor.GetComponent<Renderer>().material.SetColor("_Color", colors[random]);
        newMeteor.GetComponent<Rigidbody>().velocity = new Vector3(xValue - Random.Range(0, 9), yValue - Random.Range(0, 9), 0);
        isWaiting = true;
        islaunch = false;
    }
}
